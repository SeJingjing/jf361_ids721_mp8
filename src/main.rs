use clap::{App, Arg, ArgMatches};
use std::error::Error;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

#[derive(Debug)]
struct CountResults {
    lines: usize,
    words: usize,
    characters: usize,
    sentences: usize,
}

fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("Text File Analyzer")
        .version("1.0")
        .about("Analyzes a text file for various metrics")
        .arg(
            Arg::with_name("file")
                .short("f")
                .long("file")
                .value_name("FILE")
                .help("Sets the input text file")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("lines")
                .short("l")
                .long("lines")
                .help("Counts the number of lines"),
        )
        .arg(
            Arg::with_name("words")
                .short("w")
                .long("words")
                .help("Counts the number of words"),
        )
        .arg(
            Arg::with_name("characters")
                .short("c")
                .long("characters")
                .help("Counts the number of characters"),
        )
        .arg(
            Arg::with_name("sentences")
                .short("s")
                .long("sentences")
                .help("Counts the number of sentences"),
        )
        .get_matches();

    let file_path = matches.value_of("file").unwrap();
    let results = analyze_file(file_path)?;

    print_results(&matches, &results);

    Ok(())
}

fn analyze_file<P: AsRef<Path>>(file_path: P) -> Result<CountResults, io::Error> {
    let file = File::open(file_path)?;
    let reader = io::BufReader::new(file);

    let mut results = CountResults {
        lines: 0,
        words: 0,
        characters: 0,
        sentences: 0,
    };

    for line in reader.lines() {
        let line = line?;
        results.lines += 1;
        results.words += line.split_whitespace().count();
        results.characters += line.chars().count();
        results.sentences += line.matches(|c: char| ".!?".contains(c)).count();
    }

    Ok(results)
}

fn print_results(matches: &ArgMatches, results: &CountResults) {
    if matches.is_present("lines") {
        println!("Lines: {}", results.lines);
    }
    if matches.is_present("words") {
        println!("Words: {}", results.words);
    }
    if matches.is_present("characters") {
        println!("Characters: {}", results.characters);
    }
    if matches.is_present("sentences") {
        println!("Sentences: {}", results.sentences);
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_analyze_file() {
        let test_content = "This is a test.\nAnother sentence.";
        let test_path = "test.txt";
        std::fs::write(test_path, test_content).unwrap();

        let results = analyze_file(test_path).unwrap();
        assert_eq!(results.lines, 2);
        assert_eq!(results.words, 6);
        assert_eq!(results.characters, 32);
        assert_eq!(results.sentences, 2);

        std::fs::remove_file(test_path).unwrap();
    }

    #[test]
    fn test_analyze_file_punctuation() {
        let test_content = "Hello! How are you? I'm fine, thanks. Are you sure? Yes, 100% sure.";
        let test_path = "test_punctuation.txt";
        std::fs::write(test_path, test_content).unwrap();

        let results = analyze_file(test_path).unwrap();
        assert_eq!(results.lines, 1);
        assert_eq!(results.words, 13);
        assert_eq!(results.characters, 67);
        assert_eq!(results.sentences, 5);

        std::fs::remove_file(test_path).unwrap();
    }
}

