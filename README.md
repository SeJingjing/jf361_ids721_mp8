# jf361_ids721_mp8

 ## Project Introduction
This project involves implememting a Rust Command-Line Tool with Testing.

## Project Requirments
- Rust command-line tool
- Data ingestion/processing
- Unit tests

## Project Setup
1. Use below code to create a sample RUST funciton.
   ```angular2html
   cargo new <PROJECT-NAME>
   cd <PROJECT-NAME>
    ```

2. Implement the function in `src/main.rs` and add dependencies in `Cargo.toml`.
3. Build the project
    ```angular2html
   cargo build --release
    ```

## Project Description
This project implement the funciton that can count the number of lines, words, characters, and sentences in the text file.

### Run the RUST code
```angular2html
cargo run -- -f input.txt -l -w -c -s
```
The output after run above code:
<p> 
   <img src="screenshot/cargorun.png" /> 
</p>

Or you can also run below code on terminal to use the compiled executable directly
```angular2html
./target/release/mini_project8 -f input.txt -l -w -c -s
```
The output after run above code:
<p> 
   <img src="screenshot/compiled.png" /> 
</p>

### Unit Test
To perform unit test to test the file processing functionality, you can run below code
```angular2html
cargo test
```
The output after run above code:
<p> 
   <img src="screenshot/test.png" /> 
</p>